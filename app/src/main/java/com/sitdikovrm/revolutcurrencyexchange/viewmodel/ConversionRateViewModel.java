package com.sitdikovrm.revolutcurrencyexchange.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;

import com.sitdikovrm.revolutcurrencyexchange.model.ConversionRate;
import com.sitdikovrm.revolutcurrencyexchange.repository.ExchangeRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class ConversionRateViewModel extends AndroidViewModel {

    private static final long DELAY = 1;

    private MutableLiveData<ConversionRate> mBase = new MutableLiveData<>();
    private LiveData<Map<String, Double>> mExchangeRates;
    private LiveData<List<ConversionRate>> mConversionRates;
    private ExchangeRepository mRepository;

    private Disposable mTimer;

    public ConversionRateViewModel(@NonNull Application application) {
        super(application);

        initRepository();
        initData();
    }

    public void startTimer() {
        if (mTimer == null) {
            mTimer = Observable.interval(0, DELAY, TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(n -> updateBase())
                    .subscribe();
        }
    }

    public void stopTimer() {
        if (mTimer != null) {
            mTimer.dispose();
            mTimer = null;
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        stopTimer();
    }

    private void initRepository() {
        mRepository = new ExchangeRepository();
    }

    private void initData() {
        setBase(ConversionRate.DEFAULT_CONVERSION_RATE);
        mExchangeRates = Transformations.switchMap(mBase, this::loadExchangeRates);
        mConversionRates = Transformations.switchMap(mExchangeRates, this::loadConversionRates);
    }

    private LiveData<Map<String, Double>> loadExchangeRates(ConversionRate base) {
        return mRepository.getExchangeRates(base);
    }

    private LiveData<List<ConversionRate>> loadConversionRates(Map<String, Double> exchangeRates) {
        MutableLiveData<List<ConversionRate>> liveConversionRates = new MutableLiveData<>();

        List<ConversionRate> conversionRates = new ArrayList<>();
        ConversionRate base = mBase.getValue();
        conversionRates.add(base);

        List<ConversionRate> oldConversionRates = mConversionRates.getValue();
        if (oldConversionRates == null) {
            for (Map.Entry<String, Double> entry : exchangeRates.entrySet()) {
                String currency = entry.getKey();
                if (!currency.equals(base.getCurrency())) {
                    Double rate = entry.getValue();
                    conversionRates.add(new ConversionRate(currency, base.getRate() * rate));
                }
            }
        } else {
            for (ConversionRate conversionRate : oldConversionRates) {
                String currency = conversionRate.getCurrency();
                if (!currency.equals(base.getCurrency())) {
                    Double rate = exchangeRates.get(currency);
                    conversionRates.add(new ConversionRate(currency, base.getRate() * rate));
                }
            }
        }
        liveConversionRates.setValue(conversionRates);

        return liveConversionRates;
    }

    public LiveData<List<ConversionRate>> getConversionRates() {
        return mConversionRates;
    }

    public void setBase(ConversionRate base) {
        mBase.setValue(base);
    }

    private void updateBase() {
        setBase(getBase());
    }

    public ConversionRate getBase() {
        return mBase.getValue();
    }
}
