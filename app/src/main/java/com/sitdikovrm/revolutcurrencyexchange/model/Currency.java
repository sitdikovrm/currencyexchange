package com.sitdikovrm.revolutcurrencyexchange.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Hashtable;

public final class Currency implements Serializable {
    private static final int DEFAULT_FRACTION_DIGITS = 2;

    private final int defaultFractionDigits;
    private final String code;
    private final String country;
    private final String name;
    private final String symbol;
    private final String pathToImage;

    public static final Hashtable<String, Currency> currencies = new Hashtable<>();

    public Currency(String code, String country, String name, String symbol, String pathToImage) {
        this.defaultFractionDigits = DEFAULT_FRACTION_DIGITS;
        this.code = code;
        this.country = country;
        this.name = name;
        this.symbol = symbol;
        this.pathToImage = pathToImage;
    }

    public static Currency getInstance(String code) {
        return currencies.get(code);
    }

    public String parseRate(Double rate) {
        return new BigDecimal(rate.toString()).setScale(getDefaultFractionDigits(), BigDecimal.ROUND_HALF_DOWN).toString();
    }

    public int getDefaultFractionDigits() {
        return defaultFractionDigits;
    }

    public String getCode() {
        return code;
    }

    public String getCountry() {
        return country;
    }

    public String getName() {
        return name;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getPathToImage() {
        return pathToImage;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Currency currency = (Currency) obj;
        return this.code.equals(currency.code) && this.country.equals(currency.country) &&
                this.name.equals(currency.name) && this.symbol.equals(currency.symbol) && this.pathToImage.equals(currency.pathToImage);
    }

    @Override
    public int hashCode() {
        return this.code.concat(country).concat(name).concat(symbol).concat(pathToImage).hashCode();
    }

    @Override
    public String toString() {
        return code;
    }
}
