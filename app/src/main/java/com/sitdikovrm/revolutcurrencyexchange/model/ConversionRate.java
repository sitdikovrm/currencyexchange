package com.sitdikovrm.revolutcurrencyexchange.model;

public class ConversionRate {
    public static final ConversionRate DEFAULT_CONVERSION_RATE = new ConversionRate("EUR", 100D);

    private String currency;
    private Double rate;

    public ConversionRate(String currency, Double rate) {
        this.currency = currency;
        this.rate = rate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    @Override
    public int hashCode() {
        return currency.concat(rate.toString()).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ConversionRate conversionRate = (ConversionRate) obj;
        return this.currency.equals(conversionRate.currency) && this.rate.equals(conversionRate.rate);
    }
}
