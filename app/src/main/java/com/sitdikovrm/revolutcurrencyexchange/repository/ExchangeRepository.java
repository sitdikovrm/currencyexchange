package com.sitdikovrm.revolutcurrencyexchange.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.widget.Toast;

import com.sitdikovrm.revolutcurrencyexchange.App;
import com.sitdikovrm.revolutcurrencyexchange.R;
import com.sitdikovrm.revolutcurrencyexchange.model.Currency;
import com.sitdikovrm.revolutcurrencyexchange.model.ConversionRate;
import com.sitdikovrm.revolutcurrencyexchange.network.client.Exchange;
import com.sitdikovrm.revolutcurrencyexchange.network.response.ExchangeRatesResponse;
import com.sitdikovrm.revolutcurrencyexchange.network.service.ExchangeRatesService;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ExchangeRepository {

    private ExchangeRatesService mService;

    public ExchangeRepository() {
        initCurrencies();
    }

    private ExchangeRatesService getService() {
        if (mService == null) {
            mService = Exchange.getInstance().getService();
        }
        return mService;
    }

    public void initCurrencies() {
        Context context = App.getContext();
        Resources resources = context.getResources();
        String[] currencyCodes = resources.getStringArray(R.array.currency_code);
        String[] countryCodes = resources.getStringArray(R.array.country_code);
        String[] currencyNames = resources.getStringArray(R.array.currency_name);
        String[] currencySymbols = resources.getStringArray(R.array.currency_symbol);
        for (int i = 0; i < currencyCodes.length; ++i) {
            String currencyCode = currencyCodes[i];
            String countryCode = countryCodes[i];
            String currencyName = currencyNames[i];
            String currencySymbol = currencySymbols[i];
            String pathToImage = context.getString(R.string.path_to_assets) + String.format(context.getString(R.string.path_to_image), countryCode);
            Currency currency = new Currency(currencyCode, countryCode, currencyName, currencySymbol, pathToImage);
            Currency.currencies.put(currencyCode, currency);
        }
    }

    public LiveData<Map<String, Double>> getExchangeRates(ConversionRate base) {
        MutableLiveData<Map<String, Double>> liveData = new MutableLiveData<>();

        getService().getExchangeRates(base.getCurrency())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ExchangeRatesResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.e("TAG","On Subscribe");
                    }

                    @Override
                    public void onNext(ExchangeRatesResponse exchangeRatesResponse) {
                        Log.e("TAG","On Next");

                        Map<String, Double> data = new HashMap<>();
                        data.put(base.getCurrency(), base.getRate());
                        for (Map.Entry<String, Double> entry : exchangeRatesResponse.getRates().entrySet()) {
                            String currency = entry.getKey();
                            if(!currency.equals(base.getCurrency()) && Currency.getInstance(currency) != null) {
                                Double rate = entry.getValue();
                                data.put(currency, rate);
                            }
                        }
                        liveData.setValue(data);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("TAG","On Error");

                        Toast.makeText(App.getContext(), R.string.internet_error, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {
                        Log.e("TAG","On Complete");
                    }
                });
        return liveData;
    }
}
