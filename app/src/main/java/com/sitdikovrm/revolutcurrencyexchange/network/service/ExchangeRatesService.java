package com.sitdikovrm.revolutcurrencyexchange.network.service;

import com.sitdikovrm.revolutcurrencyexchange.network.response.ExchangeRatesResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ExchangeRatesService {

    @GET("latest")
    Observable<ExchangeRatesResponse> getExchangeRates(@Query("base") String base);
}
