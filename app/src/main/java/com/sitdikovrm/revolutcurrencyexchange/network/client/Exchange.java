package com.sitdikovrm.revolutcurrencyexchange.network.client;

import com.sitdikovrm.revolutcurrencyexchange.network.service.ExchangeRatesService;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class Exchange {

    private static final String BASE_URL = "https://revolut.duckdns.org/";

    private static Exchange instance = null;
    private OkHttpClient client;
    private Retrofit retrofit;

    private Exchange() {
        client = getClient();
        retrofit = getRetrofit();
    }

    public static Exchange getInstance() {
        if (instance == null) {
            synchronized (Exchange.class) {
                if (instance == null) {
                    instance = new Exchange();
                }
            }
        }
        return instance;
    }

    private OkHttpClient getClient() {
        if (client == null) {
            client = new OkHttpClient();
        }
        return client;
    }

    private Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(JacksonConverterFactory.create())
                    .client(getClient())
                    .build();
        }
        return retrofit;
    }

    public final ExchangeRatesService getService() {
        return getRetrofit().create(ExchangeRatesService.class);
    }
}
