package com.sitdikovrm.revolutcurrencyexchange.ui.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.MainThread;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sitdikovrm.revolutcurrencyexchange.R;
import com.sitdikovrm.revolutcurrencyexchange.model.Currency;
import com.sitdikovrm.revolutcurrencyexchange.model.ConversionRate;
import com.sitdikovrm.revolutcurrencyexchange.util.DiffCallback;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExchangeRecyclerViewAdapter extends RecyclerView.Adapter<ExchangeRecyclerViewAdapter.ViewHolder> {
    private static final String TAG = ExchangeRecyclerViewAdapter.class.getSimpleName();

    private List<ConversionRate> mConversionRates;
    private ConverterListener mListener;
    private Queue<List<ConversionRate>> pendingUpdates = new ArrayDeque<>();

    public interface ConverterListener {
        void setBase(ConversionRate base);
        void scrollToTop();
        void startTimer();
        void stopTimer();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.country_flag) ImageView countryFlag;
        @BindView(R.id.currency_code) TextView currencyCode;
        @BindView(R.id.currency_name) TextView currencyName;
        @BindView(R.id.conversion_amount) EditText conversionAmount;


        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            ButterKnife.bind(this, view);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            if (position > 0) {
                mListener.stopTimer();

                ConversionRate conversionRate = mConversionRates.get(position);
                mListener.setBase(conversionRate);

                mConversionRates.remove(position);
                mConversionRates.add(0, conversionRate);
                notifyDataSetChanged();

                mListener.scrollToTop();
                mListener.startTimer();
            }
        }
    }

    public ExchangeRecyclerViewAdapter(ConverterListener listener) {
        mListener = listener;
        mConversionRates = new ArrayList<>();
    }

    @MainThread
    public void setConversionRates(List<ConversionRate> conversionRates) {
        updateItems(conversionRates);
    }

    @Override
    public ExchangeRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_converter_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        final EditText editText = viewHolder.conversionAmount;
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (viewHolder.getAdapterPosition() == 0) {
                    Double rate = Double.parseDouble(s.toString());
                    ConversionRate base = mConversionRates.get(viewHolder.getAdapterPosition());
                    base.setRate(rate);

                    mConversionRates.set(0, base);
                    mListener.setBase(base);
                }
            }
        });

        editText.setOnKeyListener((View v, int keyCode, KeyEvent event) -> {
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN) {
                InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                return false;
            }
            return false;
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        ConversionRate conversionRate = mConversionRates.get(position);
        Currency currency = Currency.getInstance(conversionRate.getCurrency());
        String rate = currency.parseRate(conversionRate.getRate());

        Glide
                .with(viewHolder.countryFlag)
                .load(currency.getPathToImage())
                .into(viewHolder.countryFlag);

        viewHolder.currencyCode.setText(currency.getCode());
        viewHolder.currencyName.setText(currency.getName());
        viewHolder.conversionAmount.setText(rate);
    }

    @Override
    public int getItemCount() {
        return mConversionRates.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    private void applyDiffResult(List<ConversionRate> newItems, DiffUtil.DiffResult diffResult) {
        pendingUpdates.remove();
        dispatchUpdates(newItems, diffResult);
        if (pendingUpdates.size() > 0) {
            updateItemsInternal(pendingUpdates.peek());
        }
    }

    private void dispatchUpdates(List<ConversionRate> newItems, DiffUtil.DiffResult diffResult) {
        diffResult.dispatchUpdatesTo(this);
        mConversionRates.clear();
        mConversionRates.addAll(newItems);
    }

    private void updateItemsInternal(final List<ConversionRate> newItems) {
        final List<ConversionRate> oldItems = new ArrayList<>(mConversionRates);
        final Handler handler = new Handler();
        new Thread(() -> {
            final DiffUtil.DiffResult diffResult =
                    DiffUtil.calculateDiff(new DiffCallback(oldItems, newItems));
            handler.post(() -> applyDiffResult(newItems, diffResult));
        }).start();
    }

    private void updateItems(final List<ConversionRate> newItems) {
        pendingUpdates.add(newItems);
        if (pendingUpdates.size() > 1) {
            return;
        }
        updateItemsInternal(newItems);
    }
}