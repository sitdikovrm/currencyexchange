package com.sitdikovrm.revolutcurrencyexchange.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sitdikovrm.revolutcurrencyexchange.R;

public class RatesFragment extends Fragment {

    public static RatesFragment newInstance() {
        return new RatesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_rates, container, false);
    }
}
