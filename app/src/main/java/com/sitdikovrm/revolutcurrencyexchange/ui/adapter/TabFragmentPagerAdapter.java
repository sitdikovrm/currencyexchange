package com.sitdikovrm.revolutcurrencyexchange.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sitdikovrm.revolutcurrencyexchange.ui.fragment.RatesFragment;
import com.sitdikovrm.revolutcurrencyexchange.ui.fragment.ConversionRatesFragment;

public class TabFragmentPagerAdapter extends FragmentPagerAdapter {
    private static final int NUM_PAGES = 2;

    public TabFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return RatesFragment.newInstance();
            case 1:
                return ConversionRatesFragment.newInstance();
            default:
                return null;
        }
    }
}