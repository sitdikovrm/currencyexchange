package com.sitdikovrm.revolutcurrencyexchange.ui.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.sitdikovrm.revolutcurrencyexchange.R;
import com.sitdikovrm.revolutcurrencyexchange.model.ConversionRate;
import com.sitdikovrm.revolutcurrencyexchange.viewmodel.ConversionRateViewModel;
import com.sitdikovrm.revolutcurrencyexchange.ui.adapter.ExchangeRecyclerViewAdapter;

public class ConversionRatesFragment extends Fragment implements ExchangeRecyclerViewAdapter.ConverterListener {
    private static final String TAG =  ConversionRatesFragment.class.getSimpleName();

    private RecyclerView mRecyclerView;
    private ExchangeRecyclerViewAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ConversionRateViewModel mViewModel;
    private ProgressBar mSpinner;

    public static ConversionRatesFragment newInstance() {
        return new ConversionRatesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_converter, container, false);
        mSpinner = (ProgressBar) view.findViewById(R.id.progress_bar);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.converter_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter = new ExchangeRecyclerViewAdapter(this);
        mRecyclerView.setAdapter(mAdapter);

        RecyclerView.ItemAnimator animator = mRecyclerView.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        observeViewModel();
    }

    private void observeViewModel() {
        mViewModel = ViewModelProviders.of(this).get(ConversionRateViewModel.class);

        mSpinner.setVisibility(View.VISIBLE);

        mViewModel.getConversionRates().observe(this, conversionRates -> {
            if (conversionRates != null) {
                mAdapter.setConversionRates(conversionRates);
                mSpinner.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        startTimer();
    }

    @Override
    public void onPause() {
        super.onPause();

        stopTimer();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mViewModel.getConversionRates().removeObservers(this);
    }

    @Override
    public void scrollToTop() {
        mRecyclerView.scrollToPosition(0);
    }

    @Override
    public void setBase(ConversionRate base) {
        stopTimer();
        mViewModel.setBase(base);
        startTimer();
    }

    @Override
    public void startTimer() {
        mViewModel.startTimer();
    }

    @Override
    public void stopTimer() {
        mViewModel.stopTimer();
    }
}