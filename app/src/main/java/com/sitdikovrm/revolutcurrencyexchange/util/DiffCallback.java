package com.sitdikovrm.revolutcurrencyexchange.util;

import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;

import com.sitdikovrm.revolutcurrencyexchange.model.ConversionRate;

import java.util.List;

public class DiffCallback extends DiffUtil.Callback{

    private List<ConversionRate> oldConversionRates;
    private List<ConversionRate> newConversionRates;

    public DiffCallback(List<ConversionRate> newConversionRates, List<ConversionRate> oldConversionRates) {
        this.newConversionRates = newConversionRates;
        this.oldConversionRates = oldConversionRates;
    }

    @Override
    public int getOldListSize() {
        return oldConversionRates != null ? oldConversionRates.size() : 0;
    }

    @Override
    public int getNewListSize() {
        return newConversionRates != null ? newConversionRates.size() : 0;
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldConversionRates.get(oldItemPosition).getCurrency().equals(newConversionRates.get(newItemPosition).getCurrency());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldConversionRates.get(oldItemPosition).getCurrency().equals(newConversionRates.get(newItemPosition).getCurrency()) &&
                oldConversionRates.get(oldItemPosition).getRate().equals(newConversionRates.get(newItemPosition).getRate());
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}
