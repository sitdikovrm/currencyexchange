package com.sitdikovrm.revolutcurrencyexchange;

import android.app.Application;
import android.content.Context;

public class App extends Application {

    private static Application instance;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
    }

    public static Application getInstance() {
        return instance;
    }

    public static Context getContext() {
        return getInstance().getApplicationContext();
    }


}
